<?php

namespace Drupal\feeds_ldap\Feeds\Fetcher\Form;

use Drupal\feeds\FeedInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\feeds\Plugin\Type\ExternalPluginFormBase;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;

/**
 * Provides a form on the feed edit page for the SqlFetcher.
 */
class QueryFetcherFeedForm extends ExternalPluginFormBase {

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state, FeedInterface $feed = NULL) {
    $queries = \Drupal::entityTypeManager()->getStorage('ldap_query_entity')
      ->loadMultiple();
    $source_config = $feed->getConfigurationFor($this->plugin);
    $form = [];
    $form['query'] = [
      '#type' => 'select',
      '#title' => $this->t('Query'),
      '#description' => $this->t('Select the query from which to fetch the data.'),
      '#options' => array_combine(array_keys($queries), array_keys($queries)),
      '#default_value' => isset($source_config['query']) ? $source_config['query'] : 'default',
      '#required' => TRUE,
    ];
    return $form;

  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state, FeedInterface $feed = NULL) {
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state, FeedInterface $feed = NULL) {
    $feed_config = $form_state->getValues();
    $feed->setConfigurationFor($this->plugin, $feed_config);
  }

}
