<?php

namespace Drupal\feeds_ldap\Feeds\Fetcher\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\feeds\Plugin\Type\ExternalPluginFormBase;

/**
 * The configuration form for ldap fetchers.
 */
class QueryFetcherForm extends ExternalPluginFormBase {

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $queries = \Drupal::entityTypeManager()->getStorage('ldap_query_entity')
      ->loadMultiple();
    $form['queries'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Queries'),
      '#description' => $this->t('Select the queries from which to allow fetching the data.'),
      '#options' => array_combine(array_keys($queries), array_keys($queries)),
      '#default_value' => $this->plugin->getConfiguration()['queries'],
      '#required' => TRUE,
    ];

    return $form;
  }

}
