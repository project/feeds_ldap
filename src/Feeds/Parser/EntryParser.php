<?php

namespace Drupal\feeds_ldap\Feeds\Parser;

use Drupal\feeds\FeedInterface;
use Drupal\feeds\StateInterface;
use Drupal\feeds\Result\ParserResult;
use Drupal\feeds\Feeds\Item\DynamicItem;
use Drupal\feeds\Feeds\Parser\ParserBase;
use Drupal\feeds\Result\FetcherResultInterface;

/**
 * Defines an LDAP entry feed parser.
 *
 * @FeedsParser(
 *   id = "feeds_ldap_entry_parser",
 *   title = "LDAP Entry",
 *   description = @Translation("Parse an LDAP entry array."),
 * )
 */
class EntryParser extends ParserBase {

  /**
   * {@inheritdoc}
   */
  public function parse(FeedInterface $feed, FetcherResultInterface $fetcher_result, StateInterface $state) {
    // Construct the standard form of the parsed feed.
    $result = new ParserResult();
    $result->title = '';
    $result->description = '';
    $result->link = '';
    // Iterate through the fetcher results.
    foreach ($fetcher_result->getRaw() as $row) {
      $item = new DynamicItem();
      $attr = $row->getAttributes();
      if ($dn = $row->getDn()) {
        $attr['dn'] = $dn;
      }
      foreach ($feed->type->entity->getMappingSources() as $mapping_source) {
        if (isset($mapping_source['value']) && isset($attr[$mapping_source['value']])) {
          $item->set($mapping_source['machine_name'], $attr[$mapping_source['value']]);
        }
      }
      $result->addItem($item);
    }
    return $result;
  }

  /**
   * {@inheritdoc}
   */
  public function getMappingSources() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  protected function configSourceLabel() {
    return $this->t('LDAP entry');
  }

  /**
   * {@inheritdoc}
   */
  public function defaultFeedConfiguration() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [];
  }

}
