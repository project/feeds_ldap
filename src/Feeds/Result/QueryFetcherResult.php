<?php

namespace Drupal\feeds_ldap\Feeds\Result;

use Drupal\feeds\Result\FetcherResultInterface;

/**
 * Result from LDAP feeds queries.
 */
class QueryFetcherResult implements FetcherResultInterface {

  /**
   * The query result.
   *
   * @var ???
   */
  protected $results;

  /**
   * Constructor.
   */
  public function __construct($results) {
    $this->results = $results;
  }

  /**
   * Overrides parent::getRaw();.
   */
  public function getRaw() {
    return $this->results;
  }

  /**
   * {@inheritdoc}
   */
  public function getFilePath() {
    return '';
  }

}
